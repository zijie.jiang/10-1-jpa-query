package com.twuc.webApp.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigDecimal;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String> {
    // TODO
    //
    // 如果需要请在此添加方法。
    //
    // <--start--
    List<Product> findByProductLineTextDescriptionContains(String matcher);
    List<Product> findByQuantityInStockBetween(Short min, Short max);
    List<Product> findByQuantityInStockBetweenOrderByProductCodeAsc(Short min, Short max);
    List<Product> findFirst3ByQuantityInStockBetweenOrderByProductCodeAsc(Short min, Short max);
    Page<Product> findByQuantityInStockBetweenOrderByProductCodeAsc(Short min, Short max, Pageable pageable);
    // --end-->
}
